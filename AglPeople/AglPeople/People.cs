﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AglPeople
{
    public class People
    {
        public static void DisplayCatsByOwnerAndGender(List<Person> people)
        {
            var result = people.Where(w => w.Pets != null) // filter out those with no pets
                .SelectMany(s => s.Pets.Select(p => new     // select the required fields
                {
                    p.Name,
                    p.Type,
                    s.Gender
                }).Where(w => w.Type.ToUpper().Equals("CAT")))    // filter out non-cat type pets (assuming that Type will never be null)
                .OrderBy(o => o.Gender).ThenBy(o => o.Name)     // sort in alphabetical order
            .GroupBy(g => g.Gender);    // group by gender

            foreach (var p in result)
            {
                Console.WriteLine(p.Key);   // print gender
                var pets = p.ToList();
                foreach (var pet in pets)
                {
                    Console.WriteLine(String.Concat(" - ", pet.Name));      // print pet name
                }
            }
        }
    }
}
