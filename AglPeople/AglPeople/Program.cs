﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AglPeople
{
    class Program
    {
        /// <summary>
        /// Creating and initializing the HttpClient object
        /// </summary>
        static HttpClient httpClient = new HttpClient();

        /// <summary>
        /// Web service url
        /// </summary>
        static string webServiceUrl = "http://agl-developer-test.azurewebsites.net/people.json";

        static void Main(string[] args)
        {
            // block the app till the process is finished
            Run().Wait();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        static async Task Run()
        {
            // create and initialize the HttpRequestMessage
            var httpRequest = new HttpRequestMessage()
            {
                RequestUri = new Uri(webServiceUrl),
                Method = HttpMethod.Get
            };

            // setting the accept header that tells the server to send data in the specific format
            httpRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Get all people
            var people = await GetPeople(httpRequest);

            People.DisplayCatsByOwnerAndGender(people);

            Console.WriteLine("--- Press Enter to Finish ---");

            Console.ReadLine();

        }

        /// <summary>
        /// Gets the people data from server
        /// </summary>
        /// <param name="httpRequest">HTTPRequestMessage object</param>
        /// <returns>The list of people</returns>
        static async Task<List<Person>> GetPeople(HttpRequestMessage httpRequest)
        {
            List<Person> people = null;

            // send request to the server
            HttpResponseMessage response = await httpClient.SendAsync(httpRequest);

            if (response.IsSuccessStatusCode)
                // deserialize the data from response body
                people = await response.Content.ReadAsAsync<List<Person>>();

            return people;
        }
    }
}
