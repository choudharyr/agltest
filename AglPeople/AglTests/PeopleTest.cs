﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AglPeople;
using System.Collections.Generic;

namespace AglTests
{
    [TestClass]
    public class PeopleTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var Persons = new List<Person> {
                    new Person
                    {
                        Name = "Rahul",
                        Gender = "Male",
                        Pets = new List<Pet>
                        {
                            new Pet
                            {
                                Type = "Cat",
                                Name = "C1",
                            },
                            new Pet
                            {
                                Type = "Dog",
                                Name = "D1",
                            },
                            new Pet
                            {
                                Type = "CAT",
                                Name = "C2",
                            },
                        }
                    },
                    new Person
                    {
                        Name = "Rosy",
                        Gender = "Female",
                        Pets = new List<Pet>
                        {
                            new Pet
                            {
                                Type = "CaT",
                                Name = "C3",
                            },
                            new Pet
                            {
                                Type = "Fish",
                                Name = "F1",
                            },
                            new Pet
                            {
                                Type = "Rat",
                                Name = "R1",
                            },
                            new Pet
                            {
                                Type = "Cac",
                                Name = "C4",
                            },
                        }
                    },
                    new Person
                    {
                        Name = "John",
                        Gender = "Male",
                        Pets = new List<Pet>
                        {
                            new Pet
                            {
                                Type = "cat",
                                Name = "C5",
                            }
                        }
                    },
                    new Person
                    {
                        Name = "Scott",
                        Gender = "Male",
                        Pets = new List<Pet> { }
                    },
                    new Person
                    {
                        Name = "Frosty",
                        Gender = "Female"
                    },
                    new Person
                    {
                        Name = "Unknown",
                        Pets = new List<Pet>
                        {
                            new Pet
                            {
                                Type = "Snake",
                                Name = "S1"
                            },
                            new Pet
                            {
                                Type = "Cat",
                                Name = "C6"
                            }
                        }
                    }
                };

            People.DisplayCatsByOwnerAndGender(Persons);
        }
    }
}
